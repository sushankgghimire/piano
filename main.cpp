#include "piano.h"

int main()
{
	std::cout << "Piano Begins";
	Piano piano;
	while (piano.getWindowIsOpen())
	{
		piano.update();
		piano.render();
	}
	return 0;
}