#include "piano.h"

//Customizing the initial window
void Piano::initWindow() {
	this->videoMode.height = 720;
	this->videoMode.width = 1280;
	this->window = new sf::RenderWindow(this->videoMode, "Digital Piano", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize);
	this->window->setFramerateLimit(60);
	//Scaling for the entire piano
	this->scale = 2;
}

//Customizing the border for the piano
void Piano::initBorder() {
	this->notes_border.setSize(sf::Vector2f(560, 140));
	this->notes_border.setOutlineThickness(9.f);
	this->notes_border.setOutlineColor(sf::Color(37, 26, 18));

	this->notes_border.setFillColor(sf::Color(0, 0, 0, 0));
	this->notes_border.setScale(this->scale, this->scale);
	this->notes_border.setPosition(sf::Vector2f(this->window->getSize().x / 2 - this->notes_border.getSize().x * (this->scale / 2), 100));
}

//Initializing the fonts used
void Piano::initFonts() {
	if (!this->font.loadFromFile("Fonts/Georama.ttf")) {
		std::cout << "Error: Failed to load font.";
	}
	this->uiText.setFont(this->font);
}
void Piano::initFontsHeader()
{
	if (!this->font_header.loadFromFile("Fonts/Ministro.ttf")) {
		std::cout << "Error: Failed to load font.";
	}
	this->text_header.setFont(this->font_header);
	this->text_header.setString("Digital Piano");
	this->text_header.setCharacterSize(40);
	this->text_header.setPosition(sf::Vector2f(this->window->getSize().x / 2.6, 10));
	this->text_header.setFillColor(sf::Color::White);
}

//Function to return state of program. Not used rn
const bool Piano::getWindowIsOpen() const {
	return this->window->isOpen();
}

//Constructor to load the required stuff
Piano::Piano() {
	this->initWindow();
	this->initFonts();
	this->initFontsHeader();
	this->initBorder();
	this->addTiles();
}

//Destructor to end the program
Piano::~Piano() {
	delete this->window;
}

//Adding tiles to the piano
void Piano::addTiles() {
	//Stray variable to store a tile at a time
	sf::RectangleShape tile;

	//white tiles
	//C3 tile
	this->tile_identity[0].id = 1;
	this->tile_identity[0].color_id = 1;
	this->tile_identity[0].type_id = 1;
	this->tile_identity[0].shape_id = 1;
	this->tile_identity[0].name = "c3";
	this->tile_identity[0].key = "Q";
	this->tile_identity[0].key_map = sf::Keyboard::Q;
	this->relative_position = 0.f;

	//D3 tile
	this->tile_identity[1].id = 2;
	this->tile_identity[1].color_id = 1;
	this->tile_identity[1].type_id = 1;
	this->tile_identity[1].shape_id = 1;
	this->tile_identity[1].name = "d3";
	this->tile_identity[1].key = "W";
	this->tile_identity[1].key_map = sf::Keyboard::W;

	//E3 E tile
	this->tile_identity[2].id = 3;
	this->tile_identity[2].color_id = 1;
	this->tile_identity[2].type_id = 1;
	this->tile_identity[2].shape_id = 1;
	this->tile_identity[2].name = "e3";
	this->tile_identity[2].key = "E";
	this->tile_identity[2].key_map = sf::Keyboard::E;

	//f3 R
	this->tile_identity[3].id = 4;
	this->tile_identity[3].color_id = 1;
	this->tile_identity[3].type_id = 1;
	this->tile_identity[3].shape_id = 1;
	this->tile_identity[3].name = "f3";
	this->tile_identity[3].key = "R";
	this->tile_identity[3].key_map = sf::Keyboard::R;

	//G3 T
	this->tile_identity[4].id = 5;
	this->tile_identity[4].color_id = 1;
	this->tile_identity[4].type_id = 1;
	this->tile_identity[4].shape_id = 1;
	this->tile_identity[4].name = "g3";
	this->tile_identity[4].key = "T";
	this->tile_identity[4].key_map = sf::Keyboard::T;

	//A3 Y
	this->tile_identity[5].id = 6;
	this->tile_identity[5].color_id = 1;
	this->tile_identity[5].type_id = 1;
	this->tile_identity[5].shape_id = 1;
	this->tile_identity[5].name = "a3";
	this->tile_identity[5].key = "Y";
	this->tile_identity[5].key_map = sf::Keyboard::Y;

	//B3 U
	this->tile_identity[6].id = 7;
	this->tile_identity[6].color_id = 1;
	this->tile_identity[6].type_id = 1;
	this->tile_identity[6].shape_id = 1;
	this->tile_identity[6].name = "b3";
	this->tile_identity[6].key = "U";
	this->tile_identity[6].key_map = sf::Keyboard::U;

	//C4 I
	this->tile_identity[7].id = 8;
	this->tile_identity[7].color_id = 1;
	this->tile_identity[7].type_id = 1;
	this->tile_identity[7].shape_id = 1;
	this->tile_identity[7].name = "c4";
	this->tile_identity[7].key = "I";
	this->tile_identity[7].key_map = sf::Keyboard::I;

	//D4 O
	this->tile_identity[8].id = 9;
	this->tile_identity[8].color_id = 1;
	this->tile_identity[8].type_id = 1;
	this->tile_identity[8].shape_id = 1;
	this->tile_identity[8].name = "d4";
	this->tile_identity[8].key = "O";
	this->tile_identity[8].key_map = sf::Keyboard::O;

	//E4 P
	this->tile_identity[9].id = 10;
	this->tile_identity[9].color_id = 1;
	this->tile_identity[9].type_id = 1;
	this->tile_identity[9].shape_id = 1;
	this->tile_identity[9].name = "e4";
	this->tile_identity[9].key = "P";
	this->tile_identity[9].key_map = sf::Keyboard::P;

	//F4 [
	this->tile_identity[10].id = 11;
	this->tile_identity[10].color_id = 1;
	this->tile_identity[10].type_id = 1;
	this->tile_identity[10].shape_id = 1;
	this->tile_identity[10].name = "f4";
	this->tile_identity[10].key = "[";
	this->tile_identity[10].key_map = sf::Keyboard::LBracket;

	//G4 X
	this->tile_identity[11].id = 12;
	this->tile_identity[11].color_id = 1;
	this->tile_identity[11].type_id = 1;
	this->tile_identity[11].shape_id = 1;
	this->tile_identity[11].name = "g4";
	this->tile_identity[11].key = "]";
	this->tile_identity[11].key_map = sf::Keyboard::RBracket;

	//A4 C
	this->tile_identity[12].id = 13;
	this->tile_identity[12].color_id = 1;
	this->tile_identity[12].type_id = 1;
	this->tile_identity[12].shape_id = 1;
	this->tile_identity[12].name = "a4";
	this->tile_identity[12].key = "-";
	this->tile_identity[12].key_map = sf::Keyboard::Hyphen;

	// B4 M
	this->tile_identity[13].id = 14;
	this->tile_identity[13].color_id = 1;
	this->tile_identity[13].type_id = 1;
	this->tile_identity[13].shape_id = 1;
	this->tile_identity[13].name = "b4";
	this->tile_identity[13].key = "=";
	this->tile_identity[13].key_map = sf::Keyboard::Equal;

	//Black Tiles
	//C#3 tile
	this->tile_identity[14].id = 15;
	this->tile_identity[14].color_id = 2;
	this->tile_identity[14].type_id = 1;
	this->tile_identity[14].shape_id = 2;
	this->tile_identity[14].name = "c#3";
	this->tile_identity[14].key = "1";
	this->tile_identity[14].key_map = sf::Keyboard::Num1;

	//D#3 tile
	this->tile_identity[15].id = 16;
	this->tile_identity[15].color_id = 2;
	this->tile_identity[15].type_id = 1;
	this->tile_identity[15].shape_id = 2;
	this->tile_identity[15].name = "d#3";
	this->tile_identity[15].key = "2";
	this->tile_identity[15].key_map = sf::Keyboard::Num2;

	//f#3 5
	this->tile_identity[16].id = 17;
	this->tile_identity[16].color_id = 2;
	this->tile_identity[16].type_id = 1;
	this->tile_identity[16].shape_id = 2;
	this->tile_identity[16].name = "f#3";
	this->tile_identity[16].key = "3";
	this->tile_identity[16].key_map = sf::Keyboard::Num3;
	//G# 6
	this->tile_identity[17].id = 18;
	this->tile_identity[17].color_id = 2;
	this->tile_identity[17].type_id = 1;
	this->tile_identity[17].shape_id = 2;
	this->tile_identity[17].name = "g#3";
	this->tile_identity[17].key = "4";
	this->tile_identity[17].key_map = sf::Keyboard::Num4;

	//A#3 7
	this->tile_identity[18].id = 19;
	this->tile_identity[18].color_id = 2;
	this->tile_identity[18].type_id = 1;
	this->tile_identity[18].shape_id = 2;
	this->tile_identity[18].name = "a#3";
	this->tile_identity[18].key = "5";
	this->tile_identity[18].key_map = sf::Keyboard::Num5;

	//C#4 6
	this->tile_identity[19].id = 20;
	this->tile_identity[19].color_id = 2;
	this->tile_identity[19].type_id = 1;
	this->tile_identity[19].shape_id = 2;
	this->tile_identity[19].name = "c#4";
	this->tile_identity[19].key = "6";
	this->tile_identity[19].key_map = sf::Keyboard::Num6;

	//D#4 7
	this->tile_identity[20].id = 21;
	this->tile_identity[20].color_id = 2;
	this->tile_identity[20].type_id = 1;
	this->tile_identity[20].shape_id = 2;
	this->tile_identity[20].name = "d#4";
	this->tile_identity[20].key = "7";
	this->tile_identity[20].key_map = sf::Keyboard::Num7;

	//F#4 B
	this->tile_identity[21].id = 22;
	this->tile_identity[21].color_id = 2;
	this->tile_identity[21].type_id = 1;
	this->tile_identity[21].shape_id = 2;
	this->tile_identity[21].name = "f#4";
	this->tile_identity[21].key = "8";
	this->tile_identity[21].key_map = sf::Keyboard::Num8;

	//G#4 N
	this->tile_identity[22].id = 23;
	this->tile_identity[22].color_id = 2;
	this->tile_identity[22].type_id = 1;
	this->tile_identity[22].shape_id = 2;
	this->tile_identity[22].name = "g#4";
	this->tile_identity[22].key = "9";
	this->tile_identity[22].key_map = sf::Keyboard::Num9;

	// A#4 M
	this->tile_identity[23].id = 24;
	this->tile_identity[23].color_id = 2;
	this->tile_identity[23].type_id = 1;
	this->tile_identity[23].shape_id = 2;
	this->tile_identity[23].name = "a#4";
	this->tile_identity[23].key = "0";
	this->tile_identity[23].key_map = sf::Keyboard::Num0;

	//A Major key = "A";
	this->tile_identity[24].id = 25;
	this->tile_identity[24].type_id = 2;
	this->tile_identity[24].name = "A-major-chord";
	this->tile_identity[24].key = "A";
	this->tile_identity[24].key_map = sf::Keyboard::A;
	this->tile_identity[24].key_comb = "Y6P";
	this->tile_identity[24].key_combo[0] = sf::Keyboard::Y;
	this->tile_identity[24].key_combo[1] = sf::Keyboard::Num6;
	this->tile_identity[24].key_combo[2] = sf::Keyboard::P;

	// C major chord key = "S";
	this->tile_identity[25].id = 26;
	this->tile_identity[25].type_id = 2;
	this->tile_identity[25].name = "C-major-chord";
	this->tile_identity[25].key = "S";
	this->tile_identity[25].key_map = sf::Keyboard::S;
	this->tile_identity[25].key_comb = "QET";
	this->tile_identity[25].key_combo[0] = sf::Keyboard::Q;
	this->tile_identity[25].key_combo[1] = sf::Keyboard::E;
	this->tile_identity[25].key_combo[2] = sf::Keyboard::T;

	// D major chord key = "F";
	this->tile_identity[26].id = 27;
	this->tile_identity[26].type_id = 2;
	this->tile_identity[26].name = "D-major-chord";
	this->tile_identity[26].key = "D";
	this->tile_identity[26].key_map = sf::Keyboard::D;
	this->tile_identity[26].key_comb = "W3Y";
	this->tile_identity[26].key_combo[0] = sf::Keyboard::W;
	this->tile_identity[26].key_combo[1] = sf::Keyboard::Num3;
	this->tile_identity[26].key_combo[2] = sf::Keyboard::Y;

	// E major chord key = "H"
	this->tile_identity[27].id = 28;
	this->tile_identity[27].type_id = 2;
	this->tile_identity[27].name = "E-major-chord";
	this->tile_identity[27].key = "H";
	this->tile_identity[27].key_map = sf::Keyboard::H;
	this->tile_identity[27].key_comb = "E4U";
	this->tile_identity[27].key_combo[0] = sf::Keyboard::E;
	this->tile_identity[27].key_combo[1] = sf::Keyboard::Num4;
	this->tile_identity[27].key_combo[2] = sf::Keyboard::U;

	//F major chord key = "F";
	this->tile_identity[28].id = 29;
	this->tile_identity[28].type_id = 2;
	this->tile_identity[28].name = "F-major-chord";
	this->tile_identity[28].key = "F";
	this->tile_identity[28].key_map = sf::Keyboard::F;
	this->tile_identity[28].key_comb = "RYI";
	this->tile_identity[28].key_combo[0] = sf::Keyboard::R;
	this->tile_identity[28].key_combo[1] = sf::Keyboard::Y;
	this->tile_identity[28].key_combo[2] = sf::Keyboard::I;

	// G major chord key = "G";
	this->tile_identity[29].id = 30;
	this->tile_identity[29].type_id = 2;
	this->tile_identity[29].name = "G-major-chord";
	this->tile_identity[29].key = "G";
	this->tile_identity[29].key_map = sf::Keyboard::G;
	this->tile_identity[29].key_comb = "T=O";
	this->tile_identity[29].key_combo[0] = sf::Keyboard::T;
	this->tile_identity[29].key_combo[1] = sf::Keyboard::Equal;
	this->tile_identity[29].key_combo[2] = sf::Keyboard::O;

	// B major chord key = "J"
	this->tile_identity[30].id = 31;
	this->tile_identity[30].type_id = 2;
	this->tile_identity[30].name = "B-major-chord";
	this->tile_identity[30].key = "J";
	this->tile_identity[30].key_map = sf::Keyboard::J;
	this->tile_identity[30].key_comb = "U78";
	this->tile_identity[30].key_combo[0] = sf::Keyboard::U;
	this->tile_identity[30].key_combo[1] = sf::Keyboard::Num7;
	this->tile_identity[30].key_combo[2] = sf::Keyboard::Num8;

	// A minor chord key = "Z";
	this->tile_identity[31].id = 32;
	this->tile_identity[31].type_id = 2;
	this->tile_identity[31].name = "A-minor-chord";
	this->tile_identity[31].key = "Z";
	this->tile_identity[31].key_map = sf::Keyboard::Z;
	this->tile_identity[31].key_comb = "YIP";
	this->tile_identity[31].key_combo[0] = sf::Keyboard::Y;
	this->tile_identity[31].key_combo[1] = sf::Keyboard::I;
	this->tile_identity[31].key_combo[2] = sf::Keyboard::P;

	//C minor chord key = "X";
	this->tile_identity[32].id = 33;
	this->tile_identity[32].type_id = 2;
	this->tile_identity[32].name = "C-minor-chord";
	this->tile_identity[32].key = "X";
	this->tile_identity[32].key_map = sf::Keyboard::X;
	this->tile_identity[32].key_comb = "Q2T";
	this->tile_identity[32].key_combo[0] = sf::Keyboard::Q;
	this->tile_identity[32].key_combo[1] = sf::Keyboard::Num6;
	this->tile_identity[32].key_combo[2] = sf::Keyboard::T;

	//D minor key = "C"
	this->tile_identity[33].id = 34;
	this->tile_identity[33].type_id = 2;
	this->tile_identity[33].name = "D-minor-chord";
	this->tile_identity[33].key = "C";
	this->tile_identity[33].key_map = sf::Keyboard::C;
	this->tile_identity[33].key_comb = "WRY";
	this->tile_identity[33].key_combo[0] = sf::Keyboard::W;
	this->tile_identity[33].key_combo[1] = sf::Keyboard::R;
	this->tile_identity[33].key_combo[2] = sf::Keyboard::Y;

	// E minor chord key = "V";
	this->tile_identity[34].id = 35;
	this->tile_identity[34].type_id = 2;
	this->tile_identity[34].name = "E-minor-chord";
	this->tile_identity[34].key = "V";
	this->tile_identity[34].key_map = sf::Keyboard::V;
	this->tile_identity[34].key_comb = "ETU";
	this->tile_identity[34].key_combo[0] = sf::Keyboard::E;
	this->tile_identity[34].key_combo[1] = sf::Keyboard::T;
	this->tile_identity[34].key_combo[2] = sf::Keyboard::U;

	// F minor chord key = "B";
	this->tile_identity[35].id = 36;
	this->tile_identity[35].type_id = 2;
	this->tile_identity[35].name = "F-minor-chord";
	this->tile_identity[35].key = "B";
	this->tile_identity[35].key_map = sf::Keyboard::B;
	this->tile_identity[35].key_comb = "R4I";
	this->tile_identity[35].key_combo[0] = sf::Keyboard::R;
	this->tile_identity[35].key_combo[1] = sf::Keyboard::Num4;
	this->tile_identity[35].key_combo[2] = sf::Keyboard::I;

	// G minor chord key = "N";
	this->tile_identity[36].id = 37;
	this->tile_identity[36].type_id = 2;
	this->tile_identity[36].name = "G-minor-chord";
	this->tile_identity[36].key = "N";
	this->tile_identity[36].key_map = sf::Keyboard::N;
	this->tile_identity[36].key_comb = "T5O";
	this->tile_identity[36].key_combo[0] = sf::Keyboard::T;
	this->tile_identity[36].key_combo[1] = sf::Keyboard::Num5;
	this->tile_identity[36].key_combo[2] = sf::Keyboard::O;

	// b minor chord key = "M"
	this->tile_identity[37].id = 38;
	this->tile_identity[37].type_id = 2;
	this->tile_identity[37].name = "B-minor-chord";
	this->tile_identity[37].key = "M";
	this->tile_identity[37].key_map = sf::Keyboard::M;
	this->tile_identity[37].key_comb = "UO8";
	this->tile_identity[37].key_combo[0] = sf::Keyboard::U;
	this->tile_identity[37].key_combo[1] = sf::Keyboard::O;
	this->tile_identity[37].key_combo[2] = sf::Keyboard::Num8;

	//Adding all tiles to the piano. Loop for all the tiles
	for (int i = 0; i < 38; i++) {
		//Check for array with an id. Check again if its a note or a chord
		if (this->tile_identity[i].id && this->tile_identity[i].type_id == 1) {
			//White tiles
			if (i < 14) {
				tile.setPosition(this->notes_border.getPosition().x + this->relative_position * this->scale, this->notes_border.getPosition().y);
				//setting the position of tiles relative to the previous one
				if (this->tile_identity[i].shape_id == 1) {
					//customizing the tiles
					tile.setSize(sf::Vector2f(40, 140));
					tile.setOutlineThickness(0.5);
					tile.setOutlineColor(sf::Color::Black);

					tile.setOutlineColor(sf::Color::Red);
					//getting the point where the tile ends as reference for next tile.
					this->relative_position += tile.getSize().x;
				}
			}
			else {
				//Setting the position of black tiles manually
				if (i == 14) {
					this->relative_position = 30;
				}
				if (i == 15) {
					this->relative_position = 70;
				}
				if (i == 16) {
					this->relative_position = 150;
				}
				if (i == 17) {
					this->relative_position = 190;
				}
				if (i == 18) {
					this->relative_position = 230;
				}
				if (i == 19) {
					this->relative_position = 310;
				}
				if (i == 20) {
					this->relative_position = 350;
				}
				if (i == 21) {
					this->relative_position = 430;
				}
				if (i == 22) {
					this->relative_position = 470;
				}
				if (i == 23) {
					this->relative_position = 510;
				}
				//customization for black tiles
				tile.setPosition(this->notes_border.getPosition().x + this->relative_position * this->scale, this->notes_border.getPosition().y);
				tile.setSize(sf::Vector2f(20, 100));
				tile.setFillColor(sf::Color::Black);
				tile.setOutlineThickness(1);
				tile.setOutlineColor(sf::Color::Red);
			}
			tile.setScale(this->scale, this->scale);

			//setting for color of piano tiles
			if (this->tile_identity[i].color_id == 1) {
				//Try to load textures
				if (!this->texture.loadFromFile("Textures/white.png")) {
					std::cout << "File cannot be found INIT WHITE.PNG\n";
				}
				else {
					tile.setFillColor(sf::Color::White);
				}
				tile.setTexture(&this->texture);
			}

			else if (this->tile_identity[i].color_id == 2) 
			{
				tile.setFillColor(sf::Color::Black);

			}
			//adding location path to the sound file
			std::string fullPath = "sounds/";
			fullPath += this->tile_identity[i].name;
			fullPath += ".wav";
			this->tile_identity[i].location = fullPath.c_str();

			//setting wav file to buffer
			this->tile_identity[i].buffer.loadFromFile(this->tile_identity[i].location);

			//adding buffer to the sound
			this->tile_identity[i].sound.setBuffer(this->tile_identity[i].buffer);
			//Adding the tiles to the vector
			this->tiles.push_back(tile);
		}
		//getting ascii from key
		//this->tile_identity[i].ascii = this->tile_identity[i].key[0];
	}
}

//Handle the events in the piano
void Piano::pollEvents() {
	while (this->window->pollEvent(this->ev)) {
		switch (this->ev.type) {
			//Manage the end program function
		case sf::Event::Closed:
			this->window->close();
			break;
			//Manage the function triggered on key-press
		case sf::Event::KeyPressed:
			this->keyboardAction(this->ev.key.code, NULL);
			break;
			//Manage key released status
		case sf::Event::KeyReleased:
			this->key_press[this->ev.key.code] = false;
			break;
			//Manage the mouse-press functions
		case sf::Event::MouseButtonPressed:
			if (ev.mouseButton.button == sf::Mouse::Left) {
				this->mouseAction();
			}
			break;
		}
	}
}

//Keep refreshing the program with needed data
void Piano::update() {
	this->pollEvents();
	this->updateMousePositions();
	this->setDefaultColors();
}

//Mouse-press Function
void Piano::mouseAction() {
	//Same loop but ident.color_id == 1/2 to give precedence for black tile to play if clicked on its boundary
	for (size_t i = 0; i < this->tiles.size(); i++) {
		auto ident = this->tile_identity[i];
		if (ident.id) {
			if (ident.color_id == 2 && this->tiles[i].getGlobalBounds().contains(this->mousePosView)) {
				this->playSound(i);
				return;
			}
		}
	}
	//Used to avoid white tile playing instead of the overlapped black tile on mouse click.
	for (size_t i = 0; i < this->tiles.size(); i++) {
		auto ident = this->tile_identity[i];
		if (ident.id) {
			if (ident.color_id == 1 && this->tiles[i].getGlobalBounds().contains(this->mousePosView)) {
				this->playSound(i);
				return;
			}
		}
	}
}

//Draw all the piano tiles
void Piano::renderTiles(sf::RenderTarget& target) {
	for (int i = 0; i < this->tiles.size(); i++) {
		target.draw(tiles[i]);
	}
}

//Manage the key-press actions
void Piano::keyboardAction(int code, int type)
{
	//Loop for all the possible playable keybinds
	for (int i = 0; i < 38; i++)
	{
		//Check for ascii code of the pressed key in the tile_identity array
		if (this->tile_identity[i].key_map == code && !this->key_press[this->tile_identity[i].key_map])
		{
			if (type != 2) {
				this->key_press[this->tile_identity[i].key_map] = true;
			}
			//Play the note associated with the pressed key
			if (this->tile_identity[i].type_id == 1) {
				this->playSound(i);
			}
			//Play the chord associated with the pressed key
			else if (this->tile_identity[i].type_id == 2) {
				//Loop through the letters in the key combination
				for (int j = 0; j < 3; j++)
				{
					int button = this->tile_identity[i].key_combo[j];
					//Recursive function to send back to play individual piano note of the chord combination
					this->keyboardAction(button, 2);
				}
			}
		}
	}

}


//Play the sound associated
void Piano::playSound(int index) {
	//Manage the color change for the tile played
	if (tile_identity[index].shape_id == 1) {
		tiles[index].setFillColor(sf::Color::Green);
	}
	else if (tile_identity[index].shape_id == 2) {
		tiles[index].setFillColor(sf::Color::Magenta);
	}
	//Play the needed sound file
	tile_identity[index].sound.play();
}

//Check for playing sound tiles to maintain color change
void Piano::setDefaultColors() {
	for (int i = 0; i < this->tiles.size(); i++) {
		//Check if the piano tile is playing
		if (this->tile_identity[i].sound.getStatus() != 2) {
			if (this->tile_identity[i].color_id == 1) {
				tiles[i].setFillColor(sf::Color::White);
			}
			else if (this->tile_identity[i].color_id == 2) {
				tiles[i].setFillColor(sf::Color::Black);
			}
		}
	}
}

//Render everything needed in the program
void Piano::render() {
	/*
	* clears window
	* renders objects
	*/
	this->window->clear();

	//Draw game objects
	this->renderTiles(*this->window);

	//Draw the piano border
	this->window->draw(this->notes_border);

	//Draw the text in the program
	this->window->draw(this->uiText);
	this->window->draw(this->text_header);



	//Draw the key letters associated with the notes
	this->drawKeys();

	//Draw the Guide for chords
	this->majorChordGuide();
	this->minorChordGuide();

	//this->renderText(*this->window);
	this->window->display();
}

//Set position for the keys associated with the respective notes
void Piano::drawKeys() {
	sf::Text keybind;
	for (int i = 0; i < 24; i++) {
		//Key customization
		keybind.setFont(this->font);
		keybind.setString(this->tile_identity[i].key);
		keybind.setCharacterSize(20);

		//get relative position of the tile required for the letter's position
		float x_position = (this->tiles[i].getPosition().x + this->tiles[i].getSize().x / 2.6 * this->scale);
		float y_position = (this->tiles[i].getPosition().y + this->tiles[i].getSize().y / 1.3 * this->scale);
		keybind.setPosition(x_position, y_position);
		if (i < 14) {
			keybind.setFillColor(sf::Color::Black);
		}
		else {
			keybind.setFillColor(sf::Color::White);
		}

		//Draw the associated keybind on top of the note
		this->window->draw(keybind);
	}
}
void Piano::majorChordGuide() {
	sf::Text title;
	sf::Text chord;
	sf::Text key;

	int move = 25;
	int move1 = 170;
	float x_position = this->window->getSize().x / 2 - this->notes_border.getSize().x * (this->scale / 2);
	float y_position = 435;
	float x_position1 = x_position + move1;
	float y_position1 = y_position;

	title.setFont(this->font);
	title.setString(std::string("Major Chord Guide"));
	title.setCharacterSize(30);
	title.setStyle(sf::Text::Underlined | sf::Text::Bold);
	title.setPosition(x_position, 400);

	for (int i = 24; i < 31; i++) {
		//Chords
		chord.setFont(this->font);
		chord.setString(std::string((this->tile_identity[i].name)) + " ->");
		chord.setCharacterSize(20);
		chord.setStyle(sf::Text::Bold);
		chord.setFillColor(sf::Color::White);
		chord.setPosition(x_position, y_position);
		y_position += move;

		//Keys
		key.setFont(this->font);
		key.setString(this->tile_identity[i].key);
		key.setCharacterSize(20);
		key.setStyle(sf::Text::Bold);
		key.setFillColor(sf::Color::White);
		key.setPosition(x_position1, y_position1);
		if (this->key_press[this->tile_identity[i].key_map])
		{
			key.setPosition(x_position1 + 200, title.getPosition().y + 20);
			key.setCharacterSize(150);
			chord.setFillColor(sf::Color::Cyan);
			key.setFillColor(sf::Color::Cyan);
		}
		y_position1 += move;

		//Renders the guide in the screen
		this->window->draw(chord);
		this->window->draw(key);
	}
	this->window->draw(title);
}

void Piano::minorChordGuide() {
	sf::Text title;
	sf::Text chord;
	sf::Text key;

	int move = 25;
	int move1 = 170;
	float x_position = this->window->getSize().x / 2 + (this->notes_border.getSize().x / 8) * (this->scale / 2);
	float y_position = 435;
	float x_position1 = x_position + move1;
	float y_position1 = y_position;

	title.setFont(this->font);
	title.setString(std::string("Minor Chord Guide"));
	title.setCharacterSize(30);
	title.setStyle(sf::Text::Underlined | sf::Text::Bold);
	title.setPosition(x_position, 400);

	for (int i = 31; i < 38; i++) {
		//Chords
		chord.setFont(this->font);
		chord.setString(std::string((this->tile_identity[i].name)) + " ->");
		chord.setCharacterSize(20);
		chord.setStyle(sf::Text::Bold);
		chord.setFillColor(sf::Color::White);
		chord.setPosition(x_position, y_position);
		y_position += move;

		//key
		key.setFont(this->font);
		key.setString(this->tile_identity[i].key);
		key.setCharacterSize(20);
		key.setStyle(sf::Text::Bold);
		key.setFillColor(sf::Color::White);
		key.setPosition(x_position1, y_position1);
		if (this->key_press[this->tile_identity[i].key_map])
		{
			key.setPosition(x_position1 + 200, title.getPosition().y + 20);
			key.setCharacterSize(150);
			chord.setFillColor(sf::Color::Cyan);
			key.setFillColor(sf::Color::Cyan);
		}
		y_position1 += move;

		this->window->draw(chord);
		this->window->draw(key);
	}
	this->window->draw(title);
}

void Piano::updateMousePositions() {
	/*
	* @return void
	*
	* updates the mouse positions
	* vector2i
	*/
	this->mousePosWindow = sf::Mouse::getPosition(*this->window);
	this->mousePosView = this->window->mapPixelToCoords(this->mousePosWindow);
}
