#pragma once

#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"

class Tiles {
public:
	int id, color_id, type_id, ascii, shape_id;
	const char* name;
	const char* location;
	const char* key;
	int key_combo[3];
	sf::Keyboard::Key key_map;
	const char* key_comb;
	sf::SoundBuffer buffer;
	sf::Sound sound;
};

class Piano {
private:
	//Variables
	sf::RenderWindow* window;
	sf::VideoMode videoMode;
	sf::Event ev;

	//Mouse positions
	sf::Vector2i mousePosWindow;
	sf::Vector2f mousePosView;

	//Resources
	sf::Font font,font_header;
	

	//text
	sf::Text uiText,text_header;
	// Texture
	sf::Texture texture;
	sf::Sprite sprite;

	//scale for piano
	float scale;

	//piano outline
	sf::RectangleShape notes_border;
	
	std::map<int, bool> key_press;

	//Tiles
	float relative_position;
	Tiles tile_identity[50];

	std::vector<sf::RectangleShape> tiles;

	void initWindow();
	void initBorder();
	void initFonts();
	void initFontsHeader();


public:
	Piano();
	~Piano();
	const bool getWindowIsOpen() const;
	void addTiles();
	void pollEvents();
	void update();
	void renderTiles(sf::RenderTarget& target);
	void keyboardAction(int code, int type);
	void mouseAction();
	void playSound(int index);
	void setDefaultColors();
	void render();
	void majorChordGuide();
	void minorChordGuide();
	void drawKeys();
	void updateMousePositions();
};